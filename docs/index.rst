HW API
======

Welcome to the homework API documentation.


.. contents:: Table of Contents
    :local:
    :backlinks: none


.. automodule:: hwapi.routes
    :members:
    :undoc-members:
    :show-inheritance:
    :exclude-members: includeme

.. automodule:: hwapi.models
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: hwapi.views
    :members:
    :undoc-members:
    :show-inheritance:


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
