================================================================================
PD Homework API
================================================================================

Welcome to HWAPI --- a Pyramid Python web application.

This is a simple API project for total personal health care spending (in
millions of dollars) for years 1980-2009 by state and by service.

See: https://www.cms.gov/Research-Statistics-Data-and-Systems/Statistics-Trends-and-Reports/NationalHealthExpendData/NationalHealthAccountsStateHealthAccountsProvider.html

SOURCE: Centers for Medicare & Medicaid Services, Office of the Actuary,
National Health Statistics Group.


.. contents:: Table of Contents
    :local:
    :backlinks: none


--------------------------------------------------------------------------------
Getting Started
--------------------------------------------------------------------------------

Prerequisites
^^^^^^^^^^^^^

  - Python_ v2.7, or v3.4 or later.
  - pip_ v1.5 or later.


.. _Python: http://www.python.org/
.. _pip: http://pip.pypa.io/


Project Initialization
^^^^^^^^^^^^^^^^^^^^^^

Once you have correctly set up and activated your Python environment (e.g.
running ``pip -V`` should succeed with the appropriate output), you must
initialize the project and its package dependencies by running the following
``pip install`` command from the project's root directory::

    pip install -r requirements.txt -e .


Database Initialization
^^^^^^^^^^^^^^^^^^^^^^^

Before starting the app, you will need to execute the database initialization
script by running::

    hwapi-initdb development.ini

This will set up the initial database structure, as well as import data from the
included ``US_AGGREGATE09.CSV`` dataset.


--------------------------------------------------------------------------------
Running the App
--------------------------------------------------------------------------------

You can start the application with Pyramid's ``pserve`` command_, specifying the
``<environment>.ini`` configuration that you would like it to load.

In the root directory of the project, simply run::

    pserve development.ini


.. _command: http://docs.pylonsproject.org/projects/pyramid/en/latest/narr/commandline.html


--------------------------------------------------------------------------------
Documentation
--------------------------------------------------------------------------------

This application utilizes `Python docstrings`_ throughout its source code.
Documentation is written in reStructuredText_ and uses Sphinx_ for documentation
generation. The documentation generation source files can be found in the
``docs`` directory.


.. _Python docstrings: https://www.python.org/dev/peps/pep-0257/
.. _reStructuredText: http://sphinx-doc.org/rest.html
.. _Sphinx: http://sphinx-doc.org/


Building and Viewing the Docs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A recent version of the package and API documentation is hosted at at:
https://s3.amazonaws.com/pdhwapi/index.html

To build and view the docs locally, simply ``cd`` into the ``docs`` directory,
then run ``make html``. The documentation can then be viewed in the
``docs/_build/html`` directory.


--------------------------------------------------------------------------------
Testing
--------------------------------------------------------------------------------

The application's test suite can be found in the project's ``tests.py`` file.


Running the Tests
^^^^^^^^^^^^^^^^^

You can run the tests by typing::

    python setup.py test

This will run the full test suite in basic output mode. For more advanced
testing, see the next section about ``nose``.


Testing with nose
^^^^^^^^^^^^^^^^^

nose_ is a package that extends Python's basic testing functionality in various
ways. You can run nose by typing::

    nosetests


.. _nose: http://nose.readthedocs.io/
