# System imports
import os
import csv
import unittest

# 3rd party imports
import transaction
from sqlalchemy import engine_from_config
from webtest import TestApp

# Pyramid imports
from pyramid.paster import get_app, get_appsettings
from pyramid.testing import DummyRequest

# Project imports
from .models import Base, DBSession, Spending


class BaseTest(unittest.TestCase):
    def setUp(self):
        """Method called before running each test"""
        # Initialize the database
        settings = get_appsettings('test.ini')
        engine = engine_from_config(settings, 'sqlalchemy.')
        DBSession.configure(bind=engine)
        Base.metadata.bind = engine

        # Initialize the database
        Base.metadata.create_all()

        # Import CSV data into DB
        csv_path = os.path.join(settings['here'], 'US_AGGREGATE09.CSV')
        with open(csv_path) as f:
            reader = csv.DictReader(f)
            with transaction.manager:
                for row in reader:
                    DBSession.add(Spending(**row))

    def tearDown(self):
        """Method called after running each test"""
        # Remove the db session
        DBSession.remove()

        # Drop the database
        Base.metadata.drop_all()


class TestModels(BaseTest):
    def test_Spending(self):
        # cls.column_names()
        self.assertEqual(
            Spending.column_names(),
            ['id', 'Code', 'Item', 'Group', 'Region_Number', 'Region_Name',
             'State_Name', 'Y1980', 'Y1981', 'Y1982', 'Y1983', 'Y1984', 'Y1985',
             'Y1986', 'Y1987', 'Y1988', 'Y1989', 'Y1990', 'Y1991', 'Y1992',
             'Y1993', 'Y1994', 'Y1995', 'Y1996', 'Y1997', 'Y1998', 'Y1999',
             'Y2000', 'Y2001', 'Y2002', 'Y2003', 'Y2004', 'Y2005', 'Y2006',
             'Y2007', 'Y2008', 'Y2009', 'Average_Annual_Percent_Growth']
        )

        obj = DBSession.query(Spending).get(1)

        # obj.__repr__
        self.assertEqual(str(obj), '<Spending 1>')

        # obj.__json__
        json = obj.__json__(DummyRequest())
        self.assertEqual(json['id'], 1)


class TestViews(BaseTest):
    def setUp(self):
        """Method called before running each test"""
        # Set up the App
        app = get_app('test.ini', 'main')
        self.testapp = TestApp(app)

        super(self.__class__, self).setUp()

    def tearDown(self):
        """Method called after running each test"""
        # Delete the app
        del self.testapp

        super(self.__class__, self).tearDown()

    def test_root_index(self):
        # Invalid Accept header
        res = self.testapp.get('/', headers={'Accept': 'test/test'}, status=406)

        # Bare request
        res = self.testapp.get('/', status=200)
        self.assertIn('application/json', res.headers['Content-Type'])
        self.assertEqual(len(res.json['data']), 660)
        self.assertEqual(res.json['data'][0]['id'], 1)
        self.assertEqual(res.json['data'][0]['Group'], 'United States')

        # Order by region
        res = self.testapp.get('/?order_by=Region_Name', status=200)
        self.assertEqual(len(res.json['data']), 660)
        self.assertEqual(res.json['data'][0]['id'], 54)
        self.assertEqual(res.json['data'][0]['Region_Name'], 'Far West')

        # Order by region desc
        res = self.testapp.get('/?order_by=Region_Name&order=desc', status=200)
        self.assertEqual(len(res.json['data']), 660)
        self.assertEqual(res.json['data'][0]['id'], 1)
        self.assertEqual(res.json['data'][0]['Region_Name'], 'United States')

        # Invalid order_by param should order by id
        res = self.testapp.get('/?order_by=invalid', status=200)
        self.assertEqual(len(res.json['data']), 660)
        self.assertEqual(res.json['data'][0]['id'], 1)
        self.assertEqual(res.json['data'][0]['Group'], 'United States')

        # Invalid order param should order ASC
        res = self.testapp.get('/?order_by=Region_Name&order=invalid',
                               status=200)
        self.assertEqual(len(res.json['data']), 660)
        self.assertEqual(res.json['data'][0]['id'], 54)
        self.assertEqual(res.json['data'][0]['Region_Name'], 'Far West')

        # Filter by code
        res = self.testapp.get('/?code=1', status=200)
        self.assertEqual(len(res.json['data']), 60)
        self.assertEqual(res.json['data'][0]['id'], 1)
        self.assertEqual(res.json['data'][0]['Group'], 'United States')

        # Filter by a group
        res = self.testapp.get('/?group=United States', status=200)
        self.assertEqual(len(res.json['data']), 11)
        self.assertEqual(res.json['data'][0]['id'], 1)
        self.assertEqual(res.json['data'][0]['Group'], 'United States')

        # Filter by multiple groups
        res = self.testapp.get('/?group=United States&group=Region', status=200)
        self.assertEqual(len(res.json['data']), 99)
        self.assertEqual(res.json['data'][0]['id'], 1)
        self.assertEqual(res.json['data'][0]['Group'], 'United States')
        self.assertEqual(res.json['data'][1]['id'], 2)
        self.assertEqual(res.json['data'][1]['Group'], 'Region')

        # Filter by a region
        res = self.testapp.get('/?region_name=United States', status=200)
        self.assertEqual(len(res.json['data']), 11)
        self.assertEqual(res.json['data'][0]['id'], 1)
        self.assertEqual(res.json['data'][0]['Region_Name'], 'United States')

        # Filter by multiple regions
        res = self.testapp.get(
            '/?region_name=United States&region_name=New England',
            status=200
        )
        self.assertEqual(len(res.json['data']), 88)
        self.assertEqual(res.json['data'][0]['id'], 1)
        self.assertEqual(res.json['data'][0]['Region_Name'], 'United States')
        self.assertEqual(res.json['data'][1]['id'], 2)
        self.assertEqual(res.json['data'][1]['Region_Name'], 'New England')

        # Filter by a state
        res = self.testapp.get('/?state_name=South Carolina', status=200)
        self.assertEqual(len(res.json['data']), 11)
        self.assertEqual(res.json['data'][0]['id'], 39)
        self.assertEqual(res.json['data'][0]['State_Name'], 'South Carolina')

        # Filter by multiple states
        res = self.testapp.get(
            '/?state_name=South Carolina&state_name=North Carolina',
            status=200
        )
        self.assertEqual(len(res.json['data']), 22)
        self.assertEqual(res.json['data'][0]['id'], 38)
        self.assertEqual(res.json['data'][0]['State_Name'], 'North Carolina')
        self.assertEqual(res.json['data'][11]['id'], 339)
        self.assertEqual(res.json['data'][11]['State_Name'], 'South Carolina')

        # Filter by growth
        res = self.testapp.get('/?growth=5', status=200)
        self.assertEqual(len(res.json['data']), 4)
        self.assertEqual(res.json['data'][0]['id'], 434)
        self.assertEqual(res.json['data'][1]['id'], 442)
        self.assertEqual(res.json['data'][2]['id'], 445)
        self.assertEqual(res.json['data'][3]['id'], 461)

        # Filter by growth w/ lt op
        res = self.testapp.get('/?growth=lt:3.5', status=200)
        self.assertEqual(len(res.json['data']), 2)
        self.assertEqual(res.json['data'][0]['id'], 431)
        self.assertEqual(res.json['data'][1]['id'], 475)

        # Filter by growth w/ le op
        res = self.testapp.get('/?growth=le:3.1', status=200)
        self.assertEqual(len(res.json['data']), 2)
        self.assertEqual(res.json['data'][0]['id'], 431)
        self.assertEqual(res.json['data'][1]['id'], 475)

        # Filter by growth w/ gt op
        res = self.testapp.get('/?growth=gt:17', status=200)
        self.assertEqual(len(res.json['data']), 2)
        self.assertEqual(res.json['data'][0]['id'], 345)
        self.assertEqual(res.json['data'][1]['id'], 352)

        # Filter by growth w/ ge op
        res = self.testapp.get('/?growth=ge:19', status=200)
        self.assertEqual(len(res.json['data']), 2)
        self.assertEqual(res.json['data'][0]['id'], 345)
        self.assertEqual(res.json['data'][1]['id'], 352)

        # Filter by invalid growth params
        res = self.testapp.get('/?growth=test', status=200)
        self.assertEqual(len(res.json['data']), 0)
        res = self.testapp.get('/?growth=lt:', status=200)
        self.assertEqual(len(res.json['data']), 0)
        res = self.testapp.get('/?growth=gt:test', status=200)
        self.assertEqual(len(res.json['data']), 0)