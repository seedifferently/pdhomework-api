"""
Routes
------
Below is a list of valid routes for the application. You may also use Pyramid's
``proutes`` command to print a list of all routes in a console.


Root - / -- :func:`~hwapi.views.root_index`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autosummary::
   :nosignatures:

   ~hwapi.views.root_index
"""
def includeme(config):
    """
    Register the application's routes.

    cf. http://docs.pylonsproject.org/projects/pyramid/en/latest/narr/urldispatch.html#route-configuration
    """
    # GET / -> hwapi.views.root_index
    config.add_route(
        'root_index',
        pattern='/',
        request_method='GET'
    )