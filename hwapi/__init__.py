# 3rd party imports
from sqlalchemy import engine_from_config

# Pyramid imports
from pyramid.config import Configurator

# App imports
from .models import Base, DBSession
from . import routes


def main(global_config, **settings):
    """This function returns a Pyramid WSGI application."""
    # Initialize database session
    engine = engine_from_config(settings, 'sqlalchemy.')
    Base.metadata.bind = engine
    DBSession.configure(bind=engine)

    # Build config
    config = Configurator(settings=settings)

    # Run package includes
    config.include('pyramid_tm')

    # Register routes
    config.include(routes)

    # Scan for and run any other configs
    config.scan(ignore='.tests')

    return config.make_wsgi_app()