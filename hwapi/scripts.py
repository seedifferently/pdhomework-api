# Bwcompat imports
from __future__ import print_function

# System imports
import os
import sys
import csv

# 3rd party imports
import transaction
from sqlalchemy import engine_from_config

# Pyramid imports
from pyramid.paster import get_appsettings, setup_logging
from pyramid.scripts.common import parse_vars

# Project imports
from .models import Base, DBSession, Spending


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [var=value]\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def initdb(argv=sys.argv):
    # Check usage and source file
    if len(argv) < 2:
        usage(argv)

    # Initialize config
    config_uri = argv[1]
    options = parse_vars(argv[2:])
    setup_logging(config_uri)
    settings = get_appsettings(config_uri, options=options)
    # Initialize SQLAlchemy
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)

    # Create DB schema
    Base.metadata.create_all(engine)

    # Import CSV data into DB
    csv_path = os.path.join(settings['here'], 'US_AGGREGATE09.CSV')
    if not os.path.exists(csv_path):
        print('Could not find data source file: %s' % csv_path)
        sys.exit(1)

    with open(csv_path) as f:
        reader = csv.DictReader(f)
        with transaction.manager:
            for row in reader:
                DBSession.add(Spending(**row))