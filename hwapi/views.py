# -*- coding: utf-8 -*-
"""
Views
-----

**Notes:**

- Example requests are shown using `HTTPie <http://httpie.org>`_ (a
  user-friendly cURL replacement).
- Requests must have an ``Accept`` header with a value supporting
  ``application/json`` (otherwise a ``406`` response will be returned).
"""
# 3rd party imports
from sqlalchemy.sql.expression import text

# Pyramid imports
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPNotAcceptable

# Project imports
from .models import DBSession, Spending


@view_config(route_name='root_index', renderer='json')
def root_index(context, request):
    """
    :GET /: Return data for total personal health care spending (in millions \
        of dollars) for years 1980-2009.

    The following (optional) request query parameters are accepted:

    :param str order_by:      The column name to order results by.  (default:
                              ``id``)
    :param str order:         The ``order_by`` order. (default: ``ASC``)
    :param int code:          The :attr:`.Spending.Code` to filter by.
    :param str group †:       The :attr:`.Spending.Group` to filter by.
    :param str region_name †: The :attr:`.Spending.Region_Name` to filter by.
    :param str state_name †:  The :attr:`.Spending.State_Name` to filter by.
    :param growth:
        The :attr:`.Spending.Average_Annual_Percent_Growth` to filter by.
    :type growth: float or op:float

    Parameters marked with a ``†`` may be specified multiple times if desired.
    e.g. ``/?state_name=Georgia&state_name=Florida`` would return items matching
    a :attr:`.Spending.State_Name` of *either* Georgia or Florida.

    The ``growth`` parameter is unique in that it can receive either a ``float``
    or a special ``op:float`` syntax representing a comparison operator to use
    when filtering. e.g. ``/?growth=ge:8.5`` would return items with a
    :attr:`.Spending.Average_Annual_Percent_Growth` value that is *greater than
    or equal to* ``8.5``.

    Currently supported comparison operators are:

        - **lt** - Less than.
        - **le** - Less than or equal to.
        - **gt** - Greater than.
        - **ge** - Greater than or equal to.

    :returns: A JSON object containing:

        :data: A list of :class:`.Spending` objects.

    Example request::

        http -j :6543/

    Example response:

    .. code-block:: json

        {
            "data": [
                {
                    "Average_Annual_Percent_Growth": 8.1,
                    "Code": 1,
                    "Group": "United States",
                    "Item": "Personal Health Care (Millions of Dollars)",
                    "Region_Name": "United States",
                    "Region_Number": 0,
                    "State_Name": "",
                    "Y1980": 217102.314,
                    "Y1981": 251922.637,
                    "Y1982": 283230.287,
                    "Y1983": 311839.013,
                    "Y1984": 341911.961,
                    "Y1985": 376758.089,
                    "Y1986": 409365.608,
                    "Y1987": 448330.27,
                    "Y1988": 499314.393,
                    "Y1989": 551406.797,
                    "Y1990": 616599.334,
                    "Y1991": 677396.931,
                    "Y1992": 733447.651,
                    "Y1993": 780995.695,
                    "Y1994": 822982.287,
                    "Y1995": 872727.469,
                    "Y1996": 921678.737,
                    "Y1997": 974450.414,
                    "Y1998": 1028324.049,
                    "Y1999": 1088785.834,
                    "Y2000": 1164394.909,
                    "Y2001": 1264099.586,
                    "Y2002": 1371616.785,
                    "Y2003": 1478984.735,
                    "Y2004": 1585019.828,
                    "Y2005": 1692591.165,
                    "Y2006": 1798822.239,
                    "Y2007": 1904344.145,
                    "Y2008": 1997199.344,
                    "Y2009": 2089862.287,
                    "id": 1
                },
                {
                    "Average_Annual_Percent_Growth": 8.2,
                    "Code": 1,
                    "Group": "Region",
                    "Item": "Personal Health Care (Millions of Dollars)",
                    "Region_Name": "New England",
                    "Region_Number": 1,
                    "State_Name": "",
                    "Y1980": 12867.505,
                    "Y1981": 14745.225,
                    "Y1982": 16671.286,
                    "Y1983": 18328.687,
                    "Y1984": 20156.446,
                    "Y1985": 21929.723,
                    "Y1986": 24206.33,
                    "Y1987": 27050.878,
                    "Y1988": 30823.106,
                    "Y1989": 34742.121,
                    "Y1990": 38386.235,
                    "Y1991": 41394.772,
                    "Y1992": 44340.428,
                    "Y1993": 47358.024,
                    "Y1994": 49561.332,
                    "Y1995": 52770.476,
                    "Y1996": 55322.17,
                    "Y1997": 58654.237,
                    "Y1998": 62712.14,
                    "Y1999": 65388.969,
                    "Y2000": 69606.495,
                    "Y2001": 75666.938,
                    "Y2002": 82722.619,
                    "Y2003": 88914.457,
                    "Y2004": 96131.05,
                    "Y2005": 101795.349,
                    "Y2006": 108598.227,
                    "Y2007": 115531.309,
                    "Y2008": 121482.579,
                    "Y2009": 127196.402,
                    "id": 2
                }
            ]
        }
    """
    # Return a 406 if the client doesn't appear to be accepting JSON
    if 'application/json' not in request.accept:
        raise HTTPNotAcceptable

    order_by = request.GET.get('order_by', 'id')
    order_order = request.GET.get('order', 'asc').upper()
    filters = []

    # Validate ordering params
    if order_by not in Spending.column_names():
        order_by = 'id'
    if order_order not in ['ASC', 'DESC']:
        order_order = 'ASC'

    # Filter by code
    if request.GET.get('code'):
        filters.append(Spending.Code == request.GET['code'])

    # Filter by group
    if request.GET.get('group'):
        filters.append(Spending.Group.in_(request.GET.getall('group')))

    # Filter by region name
    if request.GET.get('region_name'):
        filters.append(
            Spending.Region_Name.in_(request.GET.getall('region_name'))
        )

    # Filter by state name
    if request.GET.get('state_name'):
        filters.append(
            Spending.State_Name.in_(request.GET.getall('state_name'))
        )

    # Filter by growth
    if request.GET.get('growth'):
        # Parse growth operator if utilized (and of valid length)
        if request.GET['growth'][:3] in ['lt:', 'le:', 'gt:', 'ge:'] \
           and len(request.GET['growth']) > 3:
            op, growth = request.GET['growth'].split(':', 1)
            filters.append(
                getattr(
                    Spending.Average_Annual_Percent_Growth,
                    '__%s__' % op
                )(growth)
            )
        else:
            filters.append(
                Spending.Average_Annual_Percent_Growth == request.GET['growth']
            )

    # Apply query filters
    query = DBSession.query(Spending).filter(*filters)

    # Parse ORDER BY clause
    order_clause = '%s %s' % (order_by, order_order)

    return dict(data=query.order_by(text(order_clause)).all())