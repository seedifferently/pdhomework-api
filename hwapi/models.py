"""
Models
------
"""
# 3rd party imports
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import MetaData
from sqlalchemy.orm import configure_mappers, scoped_session, sessionmaker
from sqlalchemy.schema import Column
from sqlalchemy.types import Integer, Float, Unicode
from zope.sqlalchemy import ZopeTransactionExtension

# Pyramid imports
from pyramid.threadlocal import get_current_request


# Initialize declarative base and session
metadata = MetaData()
Base = declarative_base(metadata=metadata)
DBSession = scoped_session(
    sessionmaker(extension=ZopeTransactionExtension()),
    scopefunc=get_current_request
)


class Spending(Base):
    """
    The application's Spending model.

    Represents data for total personal health care spending (in millions of
    dollars) for years 1980-2009.
    """
    __tablename__ = 'spending'

    # Columns
    id = Column(Integer, primary_key=True)
    Code = Column(Integer)
    Item = Column(Unicode)
    Group = Column(Unicode)
    Region_Number = Column(Integer)
    Region_Name = Column(Unicode)
    State_Name = Column(Unicode)
    Y1980 = Column(Float)
    Y1981 = Column(Float)
    Y1982 = Column(Float)
    Y1983 = Column(Float)
    Y1984 = Column(Float)
    Y1985 = Column(Float)
    Y1986 = Column(Float)
    Y1987 = Column(Float)
    Y1988 = Column(Float)
    Y1989 = Column(Float)
    Y1990 = Column(Float)
    Y1991 = Column(Float)
    Y1992 = Column(Float)
    Y1993 = Column(Float)
    Y1994 = Column(Float)
    Y1995 = Column(Float)
    Y1996 = Column(Float)
    Y1997 = Column(Float)
    Y1998 = Column(Float)
    Y1999 = Column(Float)
    Y2000 = Column(Float)
    Y2001 = Column(Float)
    Y2002 = Column(Float)
    Y2003 = Column(Float)
    Y2004 = Column(Float)
    Y2005 = Column(Float)
    Y2006 = Column(Float)
    Y2007 = Column(Float)
    Y2008 = Column(Float)
    Y2009 = Column(Float)
    Average_Annual_Percent_Growth = Column(Float)

    @classmethod
    def column_names(cls):
        """Return a list of the DB table's column names."""
        return [col.name for col in cls.__table__.columns]

    def __init__(self, **kw):
        for key, value in kw.items():
            setattr(self, key, value)

    def __repr__(self):
        return '<Spending %d>' % self.id

    def __json__(self, request):
        return dict(
            [(col, getattr(self, col)) for col in self.column_names()]
        )


# run configure_mappers after defining all of the models to ensure proper setup
configure_mappers()